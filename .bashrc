#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# PS1='[\u@\h \W]\$ '
#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias ll='ls -alFh'

alias pacin='sudo pacman -S'
alias pacup='sudo pacman -Syyuu'
alias pacrm='sudo pacman -Rnc'
alias pacqr='sudo pacman -Q'
alias pamac='pamac-manager'

alias mkdir='mkdir -p'
alias rmd='rm -rf'

alias vim='${EDITOR}'
alias firefox='${BROWSER}'
alias neofetch='clear && neofetch'

alias mydate='date +%D%t%I:%M\ %p'
alias sys='sudo systemctl'
alias color='surf wvanis.net &'
alias yay='yay --nodiffmenu'
alias rshift='redshift -c ~/.config/redshift.conf'

# alias rclua='nvim /home/will/.config/awesome/rc.lua'
# alias matrix='cmatrix -C cyan'
# alias bs='bullshit | lolcat'
# alias mann='bash ~/.scripts/man.sh'

export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
