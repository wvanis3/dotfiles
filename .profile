export TERMINAL=/usr/bin/kitty
export BROWSER=/usr/bin/firefox
export FILEMANAGER=/usr/bin/pcmanfm
export LAUNCHER=/usr/local/bin/dmenu_run
export EDITOR=/usr/bin/nvim
export PLAYER=/usr/bin/rhythmbox
export MAIL=/usr/bin/thunderbird
export MEDIA=/usr/bin/mpv

export PATH="$HOME/.cargo/bin:$PATH"
