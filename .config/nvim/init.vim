set nu
set ai
set si
syntax enable
set shiftwidth=2
set tabstop=2
highlight LineNr ctermfg=black ctermbg=grey
