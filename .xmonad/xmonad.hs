{- Imports -}
import XMonad
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import qualified XMonad.StackSet as W
import qualified Data.Map as M
import XMonad.Util.Run(spawnPipe)
import XMonad.Hooks.Script
import XMonad.Util.EZConfig
import System.Exit
import System.IO
import XMonad.Layout.Spacing
import XMonad.Layout.Mosaic
import XMonad.Layout.GridVariants
import XMonad.Util.SpawnOnce
import XMonad.Hooks.SetWMName

startupHookRc = do
  setWMName "XMonad"

{- Main function! -}
main = do
  xmproc <- spawnPipe "xmobar"

  xmonad $ def
    { modMask            = mod4Mask
    , borderWidth        = 2
    , terminal           = "$TERMINAL"
    , normalBorderColor  = "#000"
    , focusedBorderColor = "#aaa"
    , workspaces         = ["1","2","3","4","5","6","7","8","9"]
    , startupHook        = startupHookRc
    , manageHook         = manageDocks <+> manageHook defaultConfig
    , layoutHook         = avoidStruts  $ layoutHook defaultConfig
    -- this must be in this order, docksEventHook must be last
    , handleEventHook    = handleEventHook defaultConfig <+> docksEventHook
    , logHook            = dynamicLogWithPP xmobarPP
        { ppOutput          = hPutStrLn xmproc
        , ppTitle           = xmobarColor "white"  "" . shorten 20
        , ppHiddenNoWindows = xmobarColor "white" ""
        }
    }
    `additionalKeysP`
    [ -- Launchers
      ( "M-d",         spawn "$LAUNCHER" )
    , ( "M-<Return>",  spawn "$TERMINAL" )
    , ( "M-S-w",       spawn "$BROWSER" )
    , ( "M-f",         spawn "$FILEMANAGER" )
    , ( "M-m",         spawn "$PLAYER" )
    , ( "M-<F1>",      spawn "$MAIL" )
    , ( "M-q",         kill )
    , ( "M-<Space>",   sendMessage NextLayout )
    , ( "M-<Tab>",     windows W.focusDown )
    , ( "M-j",         windows W.focusDown )
    , ( "M-k",         windows W.focusUp )
    , ( "M-h",         sendMessage Shrink )
    , ( "M-l",         sendMessage Expand )
    , ( "M-S-j",       windows W.swapDown )
    , ( "M-S-k",       windows W.swapUp )
    , ( "M-S-r",       spawn "xmonad --recompile; xmonad --restart" )
    , ( "M-S-x",       io (exitWith ExitSuccess) ) -- Exit XMonad
    -- Brightness
    , ( "<XF86MonBrightnessUp>",     spawn "sudo brightnessctl set +10%" )
    , ( "<XF86MonBrightnessDown>",   spawn "sudo brightnessctl set 10%-" )
    -- Audio
    , ( "<XF86AudioLowerVolume>",    spawn "pamixer -d 5" )
    , ( "<XF86AudioRaiseVolume>",    spawn "pamixer -i 5" )
    , ( "<XF86AudioMute>",           spawn "pamixer -t" )
    , ( "<XF86AudioPlay>",           spawn "cmus-remote -u" )
    , ( "<XF86AudioNext>",           spawn "cmus-remote -n" )
    , ( "<XF86AudioPrev>",           spawn "cmus-remote -r" )
    -- Screenshot
    , ( "M-<Print>",                 spawn "scrot '%T_.png' -e 'mv $f /home/will/Pictures/Screenshots'" )
    --, ( "",  )
    -- Screen Lock
    , ( "M-<Escape>",                spawn "xscreensaver-command -lock")
    ]
